package pl.mariopielak.kotlin

fun main(args: Array<String>) {
    println("3 = " + recognize('3'))
}

fun recognize(c: Char) = when {
    isLetter(c) -> "Its a letter"
    isDigit(c) -> "Its a digit"
    else -> "No idea what it is"
}

fun isLetter(c: Char) = c in 'a'..'z' || c in 'A'..'Z'
fun isDigit(c: Char) = c in '0'..'9'