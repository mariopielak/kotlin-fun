package pl.mariopielak.kotlin

enum class MountainChain {
    HIMALAYA, KARAKORUM
}

class Summit(
        val name: String,
        val altitude: Int,
        val mountainChain: MountainChain

) {
    override fun toString(): String {
        return "Summit('$name', $altitude m n.p.m., $mountainChain)"
    }
}

fun main(args: Array<String>) {
    val summits = listOf(
            Summit("Mount Everest", 8848, MountainChain.HIMALAYA),
            Summit("K2", 8611, MountainChain.KARAKORUM),
            Summit("Kanczendzonga", 8586, MountainChain.HIMALAYA),
            Summit("Lhotse", 8516, MountainChain.HIMALAYA),
            Summit("Makalu", 8463, MountainChain.HIMALAYA),
            Summit("Czo Oju", 8201, MountainChain.HIMALAYA),
            Summit("Dhaulagiri", 8167, MountainChain.HIMALAYA),
            Summit("Manaslu", 8156, MountainChain.HIMALAYA),
            Summit("Nanga Parbat", 8126, MountainChain.HIMALAYA),
            Summit("Annapurna", 8091, MountainChain.HIMALAYA),
            Summit("Gaszerbrum I", 8068, MountainChain.KARAKORUM),
            Summit("Broad Peak", 8047, MountainChain.KARAKORUM),
            Summit("Gaszerbrum II", 8035, MountainChain.KARAKORUM),
            Summit("Sziszapangma", 8013, MountainChain.HIMALAYA))
    println(summits.maxBy { summit -> summit.altitude })
    println(summits.minBy { summit -> summit.altitude })
    println(summits.filter { summit -> MountainChain.KARAKORUM == summit.mountainChain })
}