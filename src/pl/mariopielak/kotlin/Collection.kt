package pl.mariopielak.kotlin


class Board (val length : Int, val name : String) {
    override fun toString(): String {
        return "Board(length=$length, name='$name')"
    }
}

class Book(val title: String, val authors: List<String>)

fun main(args: Array<String>) {
    val boards = hashSetOf(Board(152, "Burton"), Board(155, "Santa Cruz"),
            Board(155, "Rome"), Board(159, "Burton"), Board(152, "Salomon"))
    println(boards.map { board -> board.name.toUpperCase()})
    println(boards.map { board -> board.length + 10 })
    println(boards.groupBy { board -> board.length })

    val strings = listOf("abc", "def")
    println(strings.flatMap { it.toList() })


    val books = listOf(Book("Thursday Next", listOf("Jasper Fforde")),
            Book("Mort", listOf("Terry Pratchett")),
            Book("Good Omens", listOf("Terry Pratchett",
                    "Neil Gaiman")))
    println(books.flatMap { it.authors }.toSet())

}